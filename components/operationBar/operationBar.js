// compnonets/operationBar/operationBar.js
/***
 * isSearch:搜索 是否显示，showSearch，搜索操作
 * isAdd：新增 是否显示，showAdd，新增跳转函数
 * isSwitch：补货|换货 是否显示，showChange，切换补货|换货，switchType补货|换货标识
 */
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    searchX: {
      type: Number,
      value: 350
    },
    searchY: {
      type: Number,
      value: 450
    },
  },

  /**
   * 组件的初始数据
   */
  data: {
  },

  /**
   * 组件的方法列表
   */
  methods: {
    bindchange(e) {
      let detail = e.detail;
      this.setData({
        searchX: detail.x,
        searchY: detail.y
      })
    },
    //返回首页
    backHome() {
      wx.redirectTo({
        url: '../home/home',
      })
    },
  }
})
