
Component({
  properties: {
    waiting: {
      type: Boolean,
      value: false
    },
    showHint:{//是否显示提示文字
      type: Boolean,
      value:true
    },
    hintText:{//加载提示文字
      type: String,
      value:'加载中',
    }
  },

  data: {
  },
  //设置默认值
  attached: function () {
    var that = this;
  },
  lifetimes: {
    //初次创建的时候
    created: function () {

    },
  },
  pageLifetimes: {
    show: function () {
      // 页面被展示

    },
    //展示
    onshow(){

    },
    hide: function () {
      // 页面被隐藏
    },
    resize: function (size) {
      // 页面尺寸变化
    }
  },
  detached: function () {

  },

  methods: {

  }

});