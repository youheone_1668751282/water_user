// pages/activity/activity.js
const app = getApp();
let PAGE = 1;
let PAGESIZE = 5;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list: [],//获取文章列表
    empty: '',//空数据
    hasMore: '',//是否还有更多
    loading: '',//加载中
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    PAGE = 1;
    this.getNewList();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    var that = this;
    if(!that.data.hasMore){
      return;
    }
    PAGE++;
    that.getNewList();
  },


  // 打开活动
  activityDetail: function (e) {
    wx.navigateTo({
      url: '../contentdetail/contentdetail?contentid=' + e.currentTarget.dataset.contentid,
    })
  },
  //获取新闻列表
  getNewList: function () {
    var that = this;
    that.setData({
      loading: true
    })
    app.ajax({
      url: 'User/Article/articleList',
      method: "POST",
      data: { page: PAGE, row: PAGESIZE, code:app.globalData.article_column_code.HD },
      success: function (res) {
        let list = that.data.list;
        let oldList = that.data.list;
        let hasMore = true;
        let empty = false;
        if (res.data.code == 1000) {
          if(PAGE == 1){
            list = res.data.data;
          }else{
            list = [...oldList,...res.data.data];
          }
          if(res.data.data.length<PAGESIZE){
            hasMore = false;
          }
          empty = false;
        } else {
          if(PAGE == 1){
            list = [];
            empty = true;
          }
          hasMore = false;
        }
        that.setData({
          loading: false,
          hasMore: hasMore,
          empty: empty,
          list: list
        })

      }
    })
  }
})