// pages/test/test.js
var app = getApp();
let canRoll = true;//加控制，防止用户点击两次
let num = 1;//用在动画上，让用户在第二次点击的时候可以接着上次转动的角度继续转
let lotteryArrLen = 0; //放奖品的数组的长度
let lottery = []; //放奖品
let getGoodsList = '';//奖品列表
Page({
  data: {
    popup_val: false,//中奖弹窗
    // getGoodsList: '',
    user_integral: '',//用户的积分
    getDoodslist:'',//中奖列表
    prize_valueshow:'',//是否中奖
  },

  onLoad(opt) {
    let that = this;
    let aniData = wx.createAnimation({ //创建动画对象
      duration: 5500,
      timingFunction: 'ease'
    });
    that.aniData = aniData; //将动画对象赋值给this的aniData属性
  },
  /**
 * 生命周期函数--监听页面显示
 */
  onShow: function () {
    getGoodsList = '';
    this.prizeGoodsList();//获取奖品列表
  },
  //设置奖品数组 页面 抽奖列表数组重组
  setPlateData() {
     lotteryArrLen = 0;
     lotteryArrLen = lottery.length; //获取奖品数组的长度，用来判断
    // console.log("奖品数组", lottery);
    if (lotteryArrLen < 2) { //数组的奖品只有一个，扩展数组的长度到4
      let evenArr = new Array(4); //创建一个数组，方便操作。
      for (let i = 0; i < 4; i++) {
        if (i % 2 == 1) { //这里为什么要取1是为了在默认的界面将指针放在谢谢的地方，防止别人拿着中奖的截图来要奖品
          evenArr[i] = lottery[0]; //将原数组的内容赋值到新的数组
        } else {
          evenArr[i] = '谢谢' //在数组中间隔插入谢谢
        }
      }
      lottery = [...evenArr]; //将整合好的数组赋值给lottery数组
    } else { //数组中的奖品超过1个，则正常扩展数组，扩展的数组为原来的2倍
      let dataLen = 0; //用来放原来数组的索引
      let evenArr = new Array(lotteryArrLen * 2); //创建扩展数组
      for (let i = 0; i < (lotteryArrLen * 2); i++) {
        if (i % 2 == 1) {
          evenArr[i] = lottery[dataLen]; //将原来数组的值赋值给新数组
          dataLen++; //原来数组的索引加一
        } else {
          evenArr[i] = '谢谢'
        }
      }
      lottery = [...evenArr]; //将整合好的数组赋值给lottery数组
    }
    //console.log("组装后的数组", lottery)
    lotteryArrLen = lottery.length; //获取新的数组长度
    this.setData({
      lottery: lottery //设置好值，用于页面展示
    })
  },
  //开始转盘
  startRollTap() {
    if (canRoll) {
      canRoll = false;//开始抽奖将开关关闭
    let that = this;
    var sortNum = '';//中奖是第几个(仅前端页面展示)
    app.ajax({
      url: 'User/Activity/luckDraw',
      method: "POST",
      data: {},
      success: function (res) {
        if (res.data.code == 1 || res.data.code==2) {
          var integra = parseInt(that.data.user_integral)-50;//成功了就扣个50 为了展示
          var prize_value  = res.data.data.prize_value;
          that.setData({
            user_integral: integra,
            prize_valueshow: res.data.data.prize_value
          })
          //that.getuserMsg();//调取接口重新获取积分
          that.setData({
            getDoodslist: res.data.data
          })
          //索引自己组装的数组
          for (var i = 0; i < getGoodsList.length; i++) {
            if (getGoodsList[i].id == res.data.data.id && getGoodsList[i].name == res.data.data.name) {
              sortNum = i;
            }
          };
          //处理是第几个中奖数
          if (sortNum == '' || sortNum == 0) {
            sortNum = 1;
          } else{
            sortNum=(sortNum*2)+1;
          }
          //后台返回不中奖时
          if (prize_value < 1){//后台返回谢谢直接展示谢谢
            sortNum=0;
          }
            let aniData = that.aniData; //获取this对象上的动画对象
            // let rightNum = ~~(Math.random() * lotteryArrLen); //生成随机数
            //console.log(`随机数是${sortNum}`);
            let rightNum = sortNum; //设置中奖商品
            // console.log(`随机数是${rightNum}`);
            // console.log(`奖品是：${lottery[rightNum]}`);
            aniData.rotate(3600 * num - 360 / lotteryArrLen * rightNum).step(); //设置转动的圈数
            that.setData({
              aniData: aniData.export()
            })
            num++;
            // 中奖后弹出提示框
            setTimeout(function () {
              that.setData({
                popup_val: true
              })
            }, 5000);
          
        } else if (res.data.code == -2) {
          app.showToast("活动暂时没有开启", "none", 2000, function () { });
          canRoll = true;
        } else if (res.data.code == -1) {
          app.showToast("用户积分不足无法开启抽奖", "none", 2000, function () { });
          canRoll = true;
        } else if (res.data.code == 0) {
          app.showToast("抽奖失败,请稍后重试", "none", 2000, function () { });
          canRoll = true;
        }
      }
    });
   }
  },
  //立即领取
  jumpnowGet: function () {
    wx.navigateTo({
      url: '../address/address',
    })
  },
  //关闭弹窗
  close: function () {
    canRoll = true;//5庙后开关才打开
    var that=this;
    that.getuserMsg();//调取接口重新获取积分
    this.setData({
      popup_val: false
    })
  },
  //跳转中奖纪录
  jumpRecord: function () {
    wx.navigateTo({
      url: '../awardRecord/awardRecord',
    })
  },
  // 获取抽奖列表
  prizeGoodsList: function () {
    var that = this;
    app.ajax({
      url: 'User/Activity/prizeGoodsList',
      method: "POST",
      data: {},
      success: function (res) {
        if (res.data.code == 1) {
          that.getuserMsg();//获取用户积分信息~~~
          lottery = [];//将数组置空
          getGoodsList = res.data.data;
          // that.setData({
          //   getGoodsList: res.data.data
          // });
          for (var i = 0; i < res.data.data.length; i++) {
            lottery.push(res.data.data[i].name)
          }
          that.setPlateData(); //调取抽奖列表成功后才 ....执行设置转盘表面的文字
        } else {//弹窗获取奖品列表失败返回首页
          app.showToast("暂无抽奖活动,敬请期待", "none", 3000);
        }
      }
    })
  },
  //获取个人信息
  getuserMsg() {
    var that = this;
    app.ajax({
      url: 'User/User/getUser',
      method: "POST",
      data: {},
      success: function (res) {
        that.setData({
          user_integral: res.data.data.integral
        })
      }
    })
  }
})
