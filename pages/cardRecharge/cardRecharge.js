// pages/cardRecharge/cardRecharge.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    getMyCardList:'',//获取水卡列表
    is_load:false,//是否加载
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.setData({ is_load: false })
    this.getWaterCard();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },
  //跳转卡页面详情
  jumpDetail:function (){
    wx.navigateTo({
      url: '../cardDetails/cardDetails'
    })
  },
  //跳转消费记录
  jumpConsume: function (e) {
    //console.log(e.currentTarget.dataset.cardid);
    wx.navigateTo({
      url: '../expenseCalendar/expenseCalendar?card=' + e.currentTarget.dataset.cardid
    })
  },
  // 去绑定卡
  binding:function(){
  wx.navigateTo({
    url: '../binding/binding',
  })
  },
  //去充值
  toTopup:function(e){
    //console.log(e.currentTarget.dataset.cardid)
    wx.navigateTo({
      url: "../topUp/topUp?card=" + e.currentTarget.dataset.cardid,
    })
  },
  //去申请电子卡
  goapply:function(){
    wx.navigateTo({
      url: '../applyElectronicCard/applyElectronicCard'
    })
  },
  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  // 获取我的水卡
  getWaterCard:function(){
    wx.showLoading()
    var token = wx.getStorageSync('token');
    //console.log("token>>>>>", token);
    var that = this;
    app.ajax({
      url: 'User/WaterCard/waterCardList',
      method: "POST",
      data: {},
      success: function (res) {
      wx.hideLoading()
        //console.log("最后的", res.data.data)
        if(res.data.code==1000){
        that.setData({
          getMyCardList: res.data.data
        })
      }else{
          app.showToast(res.data.message, "none", 2000, function () { });
      }
        that.setData({ is_load:true})
      }
    })
  }
})