// pages/ambitusMap/ambitusMap.js
var app = getApp();
Page({
  data: {
    animationData: {},
    showModalStatus: false,
    markers: [],//标记
    polyline: [{
      points: [{
        longitude: 106.497720,
        latitude: 29.614263
      },
      {
          longitude: 106.497720,
          latitude: 29.614263
      }],
      color: "#FF0000DD",
      width: 2,
      dottedLine: true
    }],
    controls: [{
      id: 1,
      iconPath: '../../images/reMap.png',
      position: {
        left: 0,
        top: 300 - 50,
        width: 44,
        height: 44
      },
      clickable: true
    }],
    openIndex:'',
  },
  regionchange(e) {
    // console.log('拖动',e.type)
  },
  markertap(e) {
    // console.log('点一', e.detail.markerId);
    var openIndex = e.detail.markerId;
    var that=this;
    var markers=that.data.markers;
    markers[openIndex].iconPath ='../../images/mapRed.png';
    that.setData({
      markers,
      openIndex
    })
    this.openModal();
  },
  controltap(e) {
    console.log('点二', e.controlId)
  },
  onLoad(){
    var that=this;
    var pages = getCurrentPages();
    var currPage = pages[pages.length - 1];   //当前页面
    var prevPage = pages[pages.length - 2];  //上一个页面
    var getData = prevPage.data.list;
    console.log('数据信息', getData);
    var newArr = [];
    getData.forEach((item, index) => {
      newArr.push({
        title: item.village_name,
        iconPath: "../../images/map.png",
        id: index,
        zindex: -index,
        latitude: item.latitude,
        longitude: item.longitude,
        width: 30,
        height: 45,
        distance: item.distance,
        address: item.province_name == item.city_name ? item.province_name + item.area_name + item.address : item.province_name + item.city_name + item.area_name + item.address
      })
    })
    that.setData({
      markers: newArr
    })

    
  },
  onShow() {
    console.log(2)
  },
  //打开地址
  openLocatFun(){
    var that=this;
    var latitude = Number(that.data.markers[that.data.openIndex].latitude);
    var longitude = Number(that.data.markers[that.data.openIndex].longitude);
    console.log('latitude', latitude, longitude);
    wx.openLocation({
      latitude,
      longitude,
      scale: 18
    })
    // wx.getLocation({
    //   type: 'gcj02', //返回可以用于wx.openLocation的经纬度
    //   success(res) {
    //     const latitude = res.latitude
    //     const longitude = res.longitude
    //     wx.openLocation({
    //       latitude,
    //       longitude,
    //       scale: 18
    //     })
    //   }
    // })
  },
  //打开对话框
  openModal:function(){
    // 显示遮罩层
    var animation = wx.createAnimation({
      duration: 200,
      timingFunction: "linear",
      delay: 0
    })
    this.animation = animation
    animation.translateY(300).step()
    this.setData({
      animationData: animation.export(),
      showModalStatus: true
    })
    setTimeout(function () {
      animation.translateY(0).step()
      this.setData({
        animationData: animation.export()
      })
    }.bind(this), 200)

  },
  //隐藏对话框
  hideModal: function () {
    var that = this;
    var openIndex = that.data.openIndex;
    var markers = that.data.markers;
    markers[openIndex].iconPath = '../../images/map.png';
    that.setData({
      markers,
      openIndex
    })
    // 隐藏遮罩层
    var animation = wx.createAnimation({
      duration: 200,
      timingFunction: "linear",
      delay: 0
    })
    this.animation = animation
    animation.translateY(300).step()
    this.setData({
      animationData: animation.export(),
    })
    setTimeout(function () {
      animation.translateY(0).step()
      this.setData({
        animationData: animation.export(),
        showModalStatus: false
      })
    }.bind(this), 200)
  },
})