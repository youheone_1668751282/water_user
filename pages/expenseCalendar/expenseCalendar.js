// pages/expenseCalendar/expenseCalendar.js
var app=getApp();
var row_s = 20;//页码数据
var page_s = 1;//页码
Page({

  /**
   * 页面的初始数据
   */
  data: {
    getCardRecordlist:[],//获取记录
    card:'',//获取卡号
    is_request: 0, //是否到底
    hasMore: '',//是否还有更多
    loading: '',//加载中
    is_load: false,
    is_waiting: true,
  },  

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var card = options.card;
      this.setData({
        card: card
      })
      this.setData({
        allActivity: []
      })
      var page_s = 1;
      this.getCardRecord(page_s);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    var that = this;
    page_s += 1;
    var that = this;
    if (!that.data.hasMore) {
      return;
    }
    that.setData({
      loading: true
    })
    that.getCardRecord(page_s);
    // // 当没有数据时,不再请求
    // if (that.data.is_request == 1) {
    //   app.showToast("~_~ 到底了,别扯了!", "none", 2000, function () { });
    // } else {
    //   that.setData({
    //     loading: true
    //   })
    //   that.getCardRecord(page_s);
    // }
  },

  //卡的获取消费记录 User/WaterCard/getCardRecord
  getCardRecord: function (page_s){
    wx.showLoading()
    var that = this;
    var card = that.data.card; 
    //console.log("卡号", card)
    app.ajax({
      url: 'User/WaterCard/getCardRecord',
      method: "POST",
      data: { card: card, row: row_s, page: page_s},
      success: function (res) {
        wx.hideLoading()
        // console.log("获取到的结果 getCardRecordlist ",res.data.data)
        var hasMore = true;
        if(res.data.code==1000){
          var lists = "";
          if (that.data.getCardRecordlist == "") {
            lists = res.data.data;
          } else {
            var p = that.data.getCardRecordlist;
            lists = p.concat(res.data.data);
          }
          if (res.data.data.length < row_s){
            hasMore = false
          }
          console.log('hasMore', hasMore, res.data.data.length, row_s)
          lists.forEach((item,index)=>{
            item.rel_money = item.rel_money ? item.rel_money.substr(0, item.rel_money.length - 1) : item.rel_money
          })
          that.setData({
            getCardRecordlist: lists
          })
      }else{
          that.setData({
            hasMore: false,
          })
          app.showToast(res.data.message, "none", 2000, function () { });
      }
      that.setData({
        is_laod:true,
        loading: false,
        hasMore: hasMore,
        is_waiting: false
      })

      }
    })
  },
})