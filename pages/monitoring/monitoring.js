// pages/monitoring/monitoring.js
var app = getApp();
let PAGE = 1;
let PAGESIZE = 10;
let latitude = '';
let longitude = '';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list: [],//获取文章列表
    empty: '',//空数据
    hasMore: '',//是否还有更多
    loading: '',//加载中

    status: 2,//最近一台水机状态
    is_waiting:true,
    isAuthorize: true
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    PAGE = 1;
    latitude = '';
    longitude = '';
    this.getfirstStatusMsg();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },
  //跳转记录详情
  jumpDetail:function(e){
    wx.navigateTo({
      url: '../changeRecord/changeRecord?eqnumber=' + e.currentTarget.dataset.eqnumber + '&id=' + e.currentTarget.dataset.id
    })
  },
  //导航
  openLocation(e){
    var lat = Number(e.currentTarget.dataset.latitude);
    var lng = Number(e.currentTarget.dataset.longitude);
    wx.openLocation({
      latitude: lat,
      longitude: lng,
      scale: 18
    })
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    var that = this;
    if (!that.data.hasMore) {
      return;
    }
    PAGE++;
    that.getList();
  },

  

 
  //进入页面  根据经纬度  获取附近设备**************************************************************
  getfirstStatusMsg() {
    var that = this;
    // 获取经纬度
    wx.getLocation({
      type: 'wgs84',
      success: function (res) {
        that.setData({
          isAuthorize: true
        })
        latitude = res.latitude;
        longitude = res.longitude;
        that.getList();//根据经纬度获取附近水机信息
      },
      fail: function () {
        that.setData({
          isAuthorize: false
        })
        wx.hideLoading()
        app.showToast('您已取消授权位置信息,无法查看附近水质', "none", 3000, function () { });
      }
    })
  },

  //获取列表
  getList() {
    var that = this;
    that.setData({
      loading: true
    })
    app.ajax({
      url: 'User/Equipment/getNearbyEq',
      method: "POST",
      data: { 
        longitude,
        latitude,
        type: 1,
        page: PAGE, 
        pageSize: PAGESIZE 
      },
      success: function (res) {
        let list = that.data.list;
        let oldList = that.data.list;
        let hasMore = true;
        let empty = false;
        if (res.data.code == 1000) {
          if (PAGE == 1) {
            list = res.data.data.eq;
          } else {
            list = [...oldList, ...res.data.data.eq];
          }
          if (res.data.data.eq.length < PAGESIZE) {
            hasMore = false;
          }
          empty = false;
        } else {
          if (PAGE == 1) {
            list = [];
            empty = true;
          }
          hasMore = false;
        }
        that.setData({
          loading: false,
          hasMore: hasMore,
          empty: empty,
          list: list,
          is_waiting:false
        })

      }
    })
  },

  //授权位置信息
  goAuthorize() {
    var that = this;
    wx.openSetting({
      success(res) {
        if (res.authSetting['scope.userLocation']) {
          that.setData({
            isAuthorize: false
          })
          that.getfirstStatusMsg();
        }
      }
    })
  },
  //返回首页
  goHome() {
    wx.navigateBack();
  },
  
})