// pages/contentdetail/contentdetail.js
var WxParse = require('../../wxParse/wxParse.js'); 
var app=getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    contentid:'',//文章id
    allMsg:''//获取所有信息
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var contentid = options.contentid;
    this.setData({
      contentid: contentid
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getArticleDetails();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

    //获取详情
  getArticleDetails: function () {
    var that_a = this;
    var contentid = that_a.data.contentid;
    app.ajax({
      url: 'User/Article/articleDetails',
      method: "POST",
      data: { id: contentid },
      success: function (res) {
        //console.log(res.data.data);
        if(res.data.code==1000){
          that_a.setData({
            allMsg: res.data.data
          })
          WxParse.wxParse('desc', 'html', res.data.data.content, that_a, 5);
        }else{
          app.showToast("请稍后重试", "none", 2000, function () { });
        }
      }
    })
  }
})