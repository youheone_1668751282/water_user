// pages/cardDetails/cardDetails.js
var app = getApp();
Page({

    /**
     * 页面的初始数据
     */
    data: {
        cardid: '',
        cardMsg: '', //获取卡的信息
        is_load: false
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function(options) {
        var cardid = options.cardid;
        this.setData({
            cardid: cardid
        })
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function() {
        this.getWaterCardMsg();
    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function() {

    },
    //跳转消费记录
    jumpConsume: function(e) {
        wx.navigateTo({
            url: '../expenseCalendar/expenseCalendar?card=' + e.currentTarget.dataset.cardid,
        })
    },
    //去充值
    toTopup: function(e) {
        //console.log('ididididid]', e.currentTarget.dataset.cardid);
        wx.navigateTo({
            url: '../topUp/topUp?card=' + e.currentTarget.dataset.cardid,
        })
    },
    //去申请卡(实体卡)
    toApplyCard: function(e) {
        wx.navigateTo({
            url: '../applyCard/applyCard?card=' + e.currentTarget.dataset.cardid,
        })
    },
    //申请电子卡
    applynew: function() {
        wx.navigateTo({
            url: '../applyElectronicCard/applyElectronicCard',
        })
    },
    //去绑定卡
    binding: function() {
        wx.navigateTo({
            url: '../binding/binding',
        })
    },
    // 挂失水卡
    reportLoss: function() {
        var that = this;
        wx.showModal({
            title: '是否确认挂失当前水卡?',
            content: '水卡挂失后将无法使用',
            confirmColor: '#FF6565',
            success: function(res) {
                if (res.confirm) {
                    that.reportsLossFun();
                } else if (res.cancel) {}
            }
        })
    },
    // 解除挂失水卡状态
    relieve: function() {
        var that = this;
        wx.showModal({
            title: '是否确认解除水卡挂失?',
            content: '解除挂失后可立即使用',
            confirmColor: '#FF6565',
            success: function(res) {
                if (res.confirm) {
                    that.rescissionLoss();
                } else if (res.cancel) {

                }
            }
        })
    },


    //获取水卡信息 User/WaterCard/getWaterCard
    getWaterCardMsg: function() {
        var that = this;
        var cardid = that.data.cardid;
        app.ajax({
            url: 'User/WaterCard/getWaterCard',
            method: "POST",
            data: { card: cardid },
            success: function(res) {
                if (res.data.code == 1000) {
                    that.setData({
                        cardMsg: res.data.data,
                    })
                } else {
                    app.showToast(res.data.message, "none", 2000, function() {});
                }
                that.setData({ is_load: true })
            }
        })
    },
    //ajax请求    挂失
    reportsLossFun: function() {
        var that = this;
        var cardid = that.data.cardid;
        app.ajax({
            url: 'User/WaterCard/reportLoss',
            method: "POST",
            data: { card: cardid },
            success: function(res) {
                if (res.data.code == 1000) {
                    that.getWaterCardMsg()
                } else {
                    app.showToast(res.data.message, "none", 2000, function() {});
                }
            }
        })
    },
    //ajax 解除挂
    rescissionLoss: function() {
        var that = this;
        var cardid = that.data.cardid;
        app.ajax({
            url: 'User/WaterCard/rescissionLoss',
            method: "POST",
            data: { card: cardid },
            success: function(res) {
                if (res.data.code == 1000) {
                    that.getWaterCardMsg()
                } else {
                    app.showToast(res.data.message, "none", 2000, function() {});
                }
            }
        })
    },
    //返回
    backFun() {
        wx.navigateBack({
            delta: 1
        });
    }
})