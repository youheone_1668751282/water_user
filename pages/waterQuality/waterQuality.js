// pages/waterQuality/waterQuality.js
var app = getApp();
let PAGE = 1;
let PAGESIZE = 10;
let latitude = '';
let longitude = '';
Page({

  /*** 页面的初始数据****/
  data: {
    keyword: [],
    list: [],//获取文章列表
    empty: '',//空数据
    hasMore: '',//是否还有更多
    loading: '',//加载中
    is_waiting:true,
    isAuthorize: true
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    PAGE = 1;
    latitude = '';
    longitude = '';
    this.getfirstStatusMsg();
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    var that = this;
    if (!that.data.hasMore) {
      return;
    }
    PAGE++;
    that.getList();
  },

  //进入页面  根据经纬度  获取附近设备**************************************************************
  getfirstStatusMsg() {
    var that = this;
    // 获取经纬度
    wx.getLocation({
      type: 'wgs84',
      success: function (res) {
        that.setData({
          isAuthorize: true
        })
        latitude = res.latitude;
        longitude = res.longitude;
        that.getList();//根据经纬度获取附近水机信息
      },
      fail: function () {
        that.setData({
          isAuthorize: false
        })
        app.showToast('您已取消授权位置信息,无法查看附近水质', "none", 3000, function () { });
      }
    })
  },

  //获取列表
  getList() {
    var that = this;
    that.setData({
      loading: true
    })
    app.ajax({
      url: 'User/Equipment/getNearbyEq',
      method: "POST",
      data: {
        longitude,
        latitude,
        page: PAGE,
        pageSize: PAGESIZE,
        keyword: that.data.keyword,
      },
      success: function (res) {
        let list = that.data.list;
        let oldList = that.data.list;
        let hasMore = true;
        let empty = false;
        if (res.data.code == 1000) {
          if (PAGE == 1) {
            list = res.data.data.eq;
          } else {
            list = [...oldList, ...res.data.data.eq];
          }
          if (res.data.data.eq.length < PAGESIZE) {
            hasMore = false;
          }
          empty = false;
        } else {
          if (PAGE == 1) {
            list = [];
            empty = true;
          }
          hasMore = false;
        }
        that.setData({
          loading: false,
          hasMore: hasMore,
          empty: empty,
          list: list
        })
        setTimeout(() => {
          that.setData({
            loading: false,
            hasMore: hasMore,
            empty: empty,
            list: list,
            is_waiting: false });
        }, 300);
      }
    })
  },

  //导航
  openLocation(e) {
    var lat = Number(e.currentTarget.dataset.latitude);
    var lng = Number(e.currentTarget.dataset.longitude);
    wx.openLocation({
      latitude: lat,
      longitude: lng,
      scale: 18
    })
  },
  //打开地址展示
  openAdressShow() {
    wx.navigateTo({
      url: '../ambitusMap/ambitusMap',
    })
  },
  //输入园区名称搜索
  keywordInput(e){
    this.setData({
      keyword: e.detail.value
    })
  },
  //搜索
  searchList(){
    PAGE = 1;
    this.getList();
  },

  //授权位置信息
  goAuthorize(){
    var that = this;
    wx.openSetting({
      success(res) {
        if (res.authSetting['scope.userLocation']) {
          that.setData({
            isAuthorize: false
          })
          that.getfirstStatusMsg();
        }
      }
    })
  },
  //返回首页
  goHome(){
    wx.navigateBack();
  },

})