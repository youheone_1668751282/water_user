// pages/topUp/topUp.js
var app = getApp();
var on_off = true;//开关
Page({

  /**
   * 页面的初始数据
   */
  data: {
      price: '',// 选择价格
      name:1,
      checkValue:1,
      cardNum:'',//卡号
      balanceList:[],//获取金额列表
      cardInfo:'',//获取到的水卡信息
      // on_off:true,//开关
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var card = options.card;
    var that=this;
    if (card != 'undefined'){
      that.setData({
        cardNum: card
      })
    }
    if (card){
      that.getWaterCardMsg();
    }
    // console.log(that.data.cardNum);

  },
  /**
 * 生命周期函数--监听页面显示
 */
  onShow: function () {
    on_off = true;
    this.getbalanceList();
  },

  //获取水卡信息 User/WaterCard/getWaterCard
  getWaterCardMsg: function () {
    var that = this;
    var cardNum = app.trimAll(that.data.cardNum);
    app.ajax({
      url: 'User/WaterCard/getWaterCard',
      method: "POST",
      data: {
        card: cardNum
      },
      success: function (res) {
        if (res.data.code == 1000) {
          var cardInfo = res.data.data;
          cardInfo.tel=cardInfo.tel ? cardInfo.tel.replace(/^(\d{3})\d{4}(\d+)/, "$1****$2") : "暂无登记号码";
          cardInfo.balance=cardInfo.balance ? cardInfo.balance.substr(0, cardInfo.balance.length - 1):'0.00';  
          console.log('修改之后的', cardInfo)
          that.setData({
            cardInfo
          })
        } else {
          that.setData({
            cardInfo:''
          })
          app.showToast(res.data.message, "none", 2000, function () { });
        }
      }
    })
  },

  //选择扫码
  scanBtn:function(){
    // 只允许从相机扫码 result(所扫码的内容) scanType(所扫码的类型) charSet(所扫码的字符集) path(当所扫的码为当前小程序的合法二维码时，会返回此字段，内容为二维码携带的 path)
    var that=this;
    wx.scanCode({
      onlyFromCamera: true,
      success: (res) => {
        //console.log(res)
        that.setData({
          cardNum: app.formatCard(res.result)
        })
        that.getWaterCardMsg();
        if (app.formatCard(res.result).length<12){
            app.showToast('位数不能小于10位')
        }
      }
    })
  },
  //输入时修改值
  inputShow:function(e){
    this.setData({
      cardNum: app.formatCard(e.detail.value)
    })
    if (e.detail.value==''){
      app.showToast("请输入正确卡号", "none", 2000, function () { });
      this.setData({
        cardInfo:''
      })
    }
  },
  //监听失去光标时
  leaveInput:function(e){
    if (e.detail.value == ''){
      app.showToast("请输入卡号", "none", 2000, function () { });
      
    }else{
      this.getbalanceList();//重新调取金额列表
      this.getWaterCardMsg();
    }
  },
  // 选择金额
  chooseMoney: function (e) {
    console.log('????', e.currentTarget.dataset.balance);
    this.setData({
      price: e.currentTarget.dataset.balance
    })
  },
  // 单选支付方式按钮
  radioChange: function (e) {
    //console.log('选中>>>>，携带value值为：', e.detail.value)
  },

  //获取金额列表
  getbalanceList(){
    var that=this;
    app.ajax({
      url: 'User/WaterCard/give_money',//
      method: "POST",
      data: {
        card: that.data.cardNum ? app.trimAll(that.data.cardNum) : that.data.cardNum
      },
      success: function (res) {
        if (res.data.code==1000){
          var balanceList = res.data.data;
            that.setData({
              balanceList,
              price: res.data.data.money_1.recharge
            })
          }else{
          app.showToast(res.data.message, "none", 2000, function () { });
          }
      }
    })
  },
  //充值水卡
  rechargeWaterCard(){
    var that = this;
    var cardNum = app.trimAll(that.data.cardNum);
    var price = that.data.price;
    console.log("卡号",cardNum);
    if (cardNum!=''){
      if (!on_off){
          return false;
      }
      on_off = false;
    app.ajax({
      url: 'User/WaterCard/rechargeWaterCard',
      method: "POST",
      data: { card: cardNum ,money: price},
      success: function (res) {
        var arritem = res.data.data;
        if (res.data.code==1000){
          wx.requestPayment({
            'appId': arritem.appId,
            'timeStamp': arritem.timeStamp,
            'nonceStr': arritem.nonceStr,
            'package': arritem.package,
            'signType': arritem.signType,
            'paySign': arritem.paySign,
            'success': function (data) {
              app.showToast("支付成功!", "success", 2000, function () { });
              that.getWaterCardMsg();
              // setTimeout(function () {
              //   wx.navigateBack({
              //     delta: 1
              //   })
              // }, 2000);
      
            },
            'fail': function (res) {
              on_off = true;
              // that.setData({
              //   on_off: true
              // })
              app.showToast("取消支付", "none", 2000, function () { });
            }
          })
        }else{
          on_off = true;
          // that.setData({
          //   on_off: true
          // })
          app.showToast(res.data.message);
        }

      }
    })
  }else{
      app.showToast("请输入卡号", "none", 2000, function () { });
  }
  }
})