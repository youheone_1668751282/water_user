// pages/myIntegral/myIntegral.js
var app=getApp();
let PAGE = 1;//页码
let PAGESIZE = 18;//分页
Page({

  /**
   * 页面的初始数据
   */
  data: {
    userinfo:[],
    list:[],
    empty: '',//空数据
    hasMore: '',//是否还有更多
    loading:'',//加载中
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getUser();
    PAGE = 1;
    this.getIntegralList();      
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    var that = this;
    if(!that.data.hasMore){
      return;
    }
    PAGE += 1;
    that.getIntegralList();
  },

  //去抽奖
  jumpLottery:function(){
    wx.navigateTo({
      url: '../lottery/lottery',
    })
  },
  // 跳转积分规则
  ruleBtn:function(){
    wx.navigateTo({
      url: '../integralRule/integralRule',
    })
  },
  //获取我的积分列表
  getIntegralList: function (){
    var that = this;
    that.setData({
      loading: true
    })
    app.ajax({
      url: 'User/UserCenter/integralList',
      method: "POST",
      data: { page: PAGE, row :PAGESIZE },
      success: function (res) {
        let list = that.data.list;
        let oldList = that.data.list;
        let empty = false;
        let hasMore = true;
        if(res.data.code==1000){
          if(PAGE == 1){
            list = res.data.data;
          }else{
            list = oldList.concat(res.data.data);
          }
          if(res.data.data.length<PAGESIZE){
            hasMore = false;
          }
          empty = false;
        }else{
          if(PAGE == 1){
            list = [];
            empty = true;
          }else{
            empty = false;
          }
          hasMore = false;
        }
        that.setData({
          loading: false,
          hasMore: hasMore,
          empty: empty,
          list:list
        })
      }
    })
  },
  //获取用户信息
  getUser() {
    wx.showLoading()
    var that = this;
    app.ajax({
      url: "User/User/getUser",
      data: {},
      success: function (res) {
        wx.hideLoading()
        if (res.data.code == 1000) {
          that.setData({
            userinfo: res.data.data
          });   
        } else {
          app.showModal('获取用户信息失败');
        }
      }
    })
  },
})