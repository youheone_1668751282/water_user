// pages/partner/partnerApply/partnerApply.js
const app = getApp();
Page({

    /**
     * 页面的初始数据
     */
    data: {
        name:'',//姓名
        phone:'',//电话号码
        code:'',//验证码
        remarks:'',//备注
        hintText: "获取验证码",
        hintDisabled:false,//是否可点击
        isQuery:false,//是否是查询状态
        apply_status:0,//申请状态
        showPop:false,//弹窗盒子
    },

    /*** 生命周期函数--监听页面加载*/
    onLoad: function (options) {
        
    },
    /*** 生命周期函数--监听页面显示*/
    onShow: function () {
       

    },
    /**监听页面销毁*/
    onUnload: function () {
    },

    //获取短信验证码
    getSendSms() {
        if (this.data.hintDisabled) {
            return;
        }
        if (this.data.phone == "") {
            app.showToast("请输入手机号码");
            return false;
        }
        var regPhone = /^(1[3-9])\d{9}$/; 
        if(!regPhone.test(this.data.phone)){
            app.showToast("请输入正确的手机号码");
            return false;
        }
        app.ajax({
        url: "Common/Common/sendSms",
        data: {
            phone: this.data.phone
        },
        success: res => {
            if (res.data.code > 0) {
                app.showToast(res.data.message, "none", 3000);
                this.setData({
                    hintDisabled: true,
                })
                this.countDown(60);
            } else {
                app.showToast(res.data.message);
            }
        },
        error: e => {
            app.showToast("网络错误");
        }
        });
    },

    //倒计时
    countDown(times) {
        this.setData({
            hintText: times + "秒后获取",
        });
        let declineFun = setInterval(() => {
        if (times > 0) {
            this.setData({
                hintText: times + "秒后获取"
            });
            --times;
        } else {
            this.setData({
                hintText: "获取验证码",
                hintDisabled: false,
            });
            clearInterval(declineFun);
        }
        }, 1000);
    },

    //姓名&&手机号码&&验证码输入
    applyInput(e) {
        const name = e.currentTarget.dataset.name;
        this.setData({
          [name]:e.detail.value
        })
        //如果是验证码
        if(name=='code'){

        }
    },

    //提交申请
    applyBtn(){
        const that=this;
        const phone=that.data.phone;
        const remarks=that.data.remarks;
        const code=that.data.code;
        const name=that.data.name;
        if (name == "") {
            app.showToast("请输入姓名");
            return false;
        }
        if (phone == "") {
            app.showToast("请输入手机号码");
            return false;
        }
        if (code == "") {
            app.showToast("验证码不能为空");
            return false;
        }
        //多次连续触发
        wx.showLoading({
            icon:'none',
            mask: true,
        })
        app.ajax({
            url: 'User/User/applyPartner',
            data: {name,phone,remarks,code},
            success: res => {
                wx.hideLoading()
                if (res.data.code == 1000) {
                    app.showToast(res.data.message, "none", 3000);
                    setTimeout(() => {
                       wx.navigateBack({
                         delta: 1,
                       }) 
                    }, 3000);
                }else{
                    app.showToast(res.data.message, "none", 3000);
                }
            },
            error: e => {
                app.showToast("网络错误");
                wx.hideLoading()
            }
        })
    },
    //切换查询//申请
    switchFun(){
        this.setData({
            isQuery:!this.data.isQuery
        })
        const version = wx.getSystemInfoSync().SDKVersion;
        if(this.data.isQuery){
            wx.setNavigationBarTitle({
              title: '查询申请状态',
            })
            //返回上级页面询问弹窗
            app.compareVersion(version, '2.12.0') >= 0&&wx.enableAlertBeforeUnload({
                message:"是否取消申请?",
                success:res=>{
                    console.log('ok',res)
                },
                fail:e=>{
                    console.log('数据结果',e);
                }
            })
        }else{
            //返回关闭返回上级页面询问弹窗
            app.compareVersion(version, '2.12.0') >= 0&&wx.disableAlertBeforeUnload();
            wx.setNavigationBarTitle({
                title: '申请合伙人',
            })
        }
    },

    //查询申请状态
    queryBtn(){
        const that=this;
        const tel=that.data.phone;
        if (tel == "") {
            app.showToast("请输入手机号码");
            return false;
        }
        var regPhone = /^(1[3-9])\d{9}$/; 
        if(!regPhone.test(tel)){
            app.showToast("请输入正确的手机号码");
            return false;
        }
        app.ajax({
            url: 'User/User/applyDetail',
            data: {tel},
            success: res => {
                if (res.data.code == 1000) {
                    that.setData({
                        showPop:true,
                        apply_status:res.data.data.apply_status,//apply_status 1申请中  2申请通过  3申请拒绝',
                    })
                }else if(res.data.code == -2000){
                    wx.showModal({
                        title: '提示',
                        content: '暂时还未提交申请，是否去申请?',
                        success (res) {
                          if (res.confirm) {
                            console.log('用户点击确定')
                            that.setData({
                                isQuery:!that.data.isQuery
                            })
                          } else if (res.cancel) {
                            console.log('用户点击取消')
                          }
                        }
                      })
                }else{
                    app.showToast(res.data.message, "none", 3000);
                    that.setData({
                        apply_status:0
                    })
                }
            },
            error: e => {
                app.showToast("网络错误");
            }
        })
    },

    //关闭弹窗
    closePop(){
        this.setData({
            showPop:false,
        })
    }
    
})