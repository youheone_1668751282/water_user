// pages/login/login.js
var app = getApp();
var countdown = 120;
var reget = '获取验证码';
var settime = function (that) {
  if (countdown == 0) {
    that.setData({
      is_show: true
    })
    countdown = 120;
    reget='重新获取';
    return;
  } else {
    that.setData({
      is_show: false,
      last_time: countdown
    })
    countdown--;
    reget = '重新获取';
  }
  setTimeout(function () {
    settime(that)
  }
    , 1000)
}
Page({

  /**
   * 页面的初始数据
   */
  data: {
    moveStartX: 0, //起始位置
    moveSendBtnLeft: 0, //发送按钮的left属性
    moveEndX: 0, //结束位置
    screenWidth: 0, //屏幕宽度
    moveable: true, //是否可滑动
    disabled: false,//验证码输入框是否可用,
    SendBtnColor: "#7f7f7f", //滑动按钮颜色
    phone: '',
    code:'',
    getOnecode: reget,//内容
    last_time: '',//剩余时间
    is_show: true,//是否显示倒计时
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    var phone = wx.getStorageSync('phone');
    that.setData({
      phone: phone
    })
    // 获取屏幕宽度
    wx.getSystemInfo({
      success: function (res) {
        that.setData({
          screenWidth: res.screenWidth
        })
      },
    })
  },
  //  开始移动
  moveSendBtnStart: function (e) {
    if (!this.data.moveable) {
      return;
    }
    // console.log("start");
    // console.log(e);
    this.setData({
      moveStartX: e.changedTouches["0"].clientX
    })
  },
  //移动发送按钮
  moveSendBtn: function (e) {
    if (!this.data.moveable) {
      return;
    }
    var that = this;
    // console.log(e.touches[0]);
    var left = ((e.touches[0].clientX - that.data.moveStartX) / (that.data.screenWidth / 750))
    //console.log("左边", left)
    if (left <= 500 && left >= 0) {
      this.setData({
        moveSendBtnLeft: left,

      })
    } else {
      this.setData({
        moveSendBtnLeft: 500
      })
    }
  },
  // 结束移动
  moveSendBtnEnd: function (e) {
    //console.log("end结束移动");
    var that = this;
    var left = ((e.changedTouches[0].clientX - that.data.moveStartX) / (that.data.screenWidth / 750))
    //console.log(left);
    if (left < 500) {
      for (let i = left; i >= 0; i--) {

        that.setData({
          moveSendBtnLeft: i,
          moveable: false
        })
      }
    } else {
      that.setData({
        moveEndX: e.changedTouches[0].clientX,
        moveable: false,
        disabled: false,
        SendBtnColor: "#289adc"
      })
    }
  },
  setPhone(e) {
    this.setData({
      phone: e.detail.value,
    });
  },
  setCode(e) {
    this.setData({
      code: e.detail.value,
    });
  },
  /**
   * 发送验证码
   */
  sendCode() {
    var that = this;
    if (that.data.phone == '') {
      app.showToast('手机号码不能为空'); return false;
    }
    if (!that.isPoneAvailable(that.data.phone)){
      app.showToast('手机号码格式不正确'); return false;
    }
     app.ajax({
       url:'User/Login/send_sms',
       data:{
         phone:that.data.phone,
       },
       success:function(res){
         that.setData({
           is_show: (!that.data.is_show)  //false
         })
         settime(that); //刷新倒计时 
         that.setData({
           getOnecode: reget        
         })   
         app.showToast(res.data.message, "none", 2500, function () { });   
       }
       
     }) 

  },
  isPoneAvailable(str) {
    var myreg = /^[1][3,4,5,7,8][0-9]{9}$/;
    if (!myreg.test(str)) {
      return false;
    } else {
      return true;
    }
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },
  //跳转主页 绑定
  jumphome: function () {
    var that = this;
    if (that.data.phone == '') {
      app.showToast('手机号码不能为空'); return false;
    }
    if (!that.isPoneAvailable(that.data.phone)) {
      app.showToast('手机号码格式不正确'); return false;
    }
    if(that.data.code==''){
      app.showToast('请输入验证码'); return false;
    }
    wx.setStorageSync('phone', that.data.phone);
    wx.setStorageSync('is_way', 2);
    app.getToken(that.data.code);
 
    // app.ajax({
    //   url: 'User/Login/bind_phone',
    //   data: {
    //     phone: that.data.phone,
    //     code:that.data.code
    //   },
    //   success: function (res) {     
    //     wx.setStorageSync('phone', that.data.phone);   
    //     app.showToast(res.data.message);
    //     if(res.data.code==1000){
    //         setTimeout(function(){
    //           wx.redirectTo({
    //             url: '../home/home',
    //           })
    //         },1000);
    //         return true;
    //     }
    //   }

    // }) 

    
  },
})