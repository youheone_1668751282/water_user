// pages/myOrderList/myOrderList.js
var app = getApp();
let PAGE = 1;
let PAGESIZE = 10;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list: [], //列表数据
    empty: '', //空数据
    hasMore: '', //是否还有更多
    loading: '', //加载中
    have: '', //是否有返回按钮
    is_waiting:true
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    PAGE = 1;
    this.getOrder();
    this.setData({
      have: options.have||'',
      list: [], //列表数据
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {
    this.setData({
      list: [], //列表数据
      hasMore:'',
      loading:'',
    })
    PAGE=1;
    this.getOrder();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {
    var that = this;
    if(!that.data.hasMore){
      return;
    }
    PAGE ++;
    that.getOrder();
  },
  //返回首页
  backHome() {
    wx.redirectTo({
      url: '../home/home',
    })
  },
  //获取我的订单
  getOrder() {
    var that = this;
    that.setData({
      loading: true
    })
    app.ajax({
      url: "User/UserCenter/orderList",
      data: {
        page: PAGE,
        row: PAGESIZE
      },
      success: function(res) {
        let list = that.data.list;
        let oldList = that.data.list;
        let hasMore = true;
        let empty = false;
        if(res.data.code == 1000){
          if(PAGE == 1){
            list = res.data.data;
          }else{
            list = [...oldList,...res.data.data];
          }
          if(res.data.data.length<PAGESIZE){
            hasMore = false;
          }
          empty = false;
        }else{
          if(PAGE == 1){
            list = [];
            empty = true;
          }
          hasMore = false;
        }
        that.setData({
          loading: false,
          hasMore: hasMore,
          empty: empty,
          list: list,
          is_waiting:false
        })
        setTimeout(()=>{
          wx.stopPullDownRefresh()
        },1000)
      }
    })
  },
  //查看订单详情
  lookOrder(e) {
    wx.navigateTo({
      url: '../orderDetail/orderDetail?id=' + e.currentTarget.dataset.id,
    })
  },
})