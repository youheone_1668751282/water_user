// pages/setting/setting.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    phone:'',
    user:'',
    is_load:false
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
      this.setData({
        phone: options.phone ? options.phone:''
      })
  },
  onShow(){
    this.getUser(); 
  },
  //退出登录
  outLogin(){
    var that=this;
    wx.showModal({
      title: '是否退出登录',
      content: '  ',
      confirmColor:'#1CBFFF',
      success(res) {
        if (res.confirm) {
          console.log('用户点击确定')
          that.removeLoginMsg();
        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })
  },
  removeLoginMsg(){
    var that=this;
    wx.setStorageSync('is_login', false);
    wx.setStorageSync('is_first', true);
    wx.setStorageSync('phone', '');
    wx.setStorageSync('token', '');
    wx.setStorageSync('nickName', '');
    wx.setStorageSync('openid', '');
    wx.reLaunch({
      url: '/pages/loginChoose/loginChoose',
    })
  },
  //获取用户信息
  getUser() {
    var that = this;
    app.ajax({
      url: "User/User/getUser",
      data: {},
      success: function (res) {
        if (res.data.code == 1000) {
          that.setData({
            user: res.data.data,
            phone: res.data.data.phone,
          })
          wx.setStorageSync('user_id', res.data.data.user_id)
          var phone = res.data.data.phone;
          wx.setStorageSync('phone', phone);
          if (phone == '') {
            that.setData({
              popup: true
            })
            return;
          }

        } else {
          wx.setStorageSync('token', '');
          // app.getToken(); 
          that.setData({
            popup: true
          })
        }
        that.setData({
          is_load: true
        })
      }
    })
  },
  // 选择头像
  chooseimage: function () {
    var that = this;
    wx.showActionSheet({
      itemList: ['从相册中选择', '拍照'],
      itemColor: "#4EB7FF",
      success: function (res) {
        if (!res.cancel) {
          if (res.tapIndex == 0) {
            that.chooseWxImage('album')
          } else if (res.tapIndex == 1) {
            that.chooseWxImage('camera')
          }
        }
      }
    })
  },
  // 头像 上传路径
  chooseWxImage: function (type) {
    var that = this;
    wx.chooseImage({
      count: 1,
      sizeType: ['original', 'compressed'],
      sourceType: [type],
      success: function (res) {
        wx.uploadFile({
          url: app.globalData._url + 'Common/Common/upload',
          filePath: res.tempFilePaths[0],
          name: 'file',
          success: function (res) {
            var data = JSON.parse(res.data);
            if (data.code == 1000) {
              that.editUserImg(data.data.url, data.data.all_url); //调用修改接口
            } else {
              app.showToast("上传头像失败");
            }
          }
        })
      }
    })
  },
  // 修改头像 调用接口............
  editUserImg(headimg, all_url) {
    var that = this;
    app.ajax({
      url: 'User/User/editUserInfo',
      data: {
        avatar_img: all_url,
      },
      success: function (res) {
        if (res.data.code == 1000) {
          app.showToast(res.data.message);
          var user = that.data.user;
          user.headimg = all_url;
          that.setData({
            user: user,
          })
        }

      }
    })
  },
  //修改手机号码
  changePhone(){
    wx.navigateTo({
      url: '../changePhone/changePhone?phone=' + this.data.phone,
    })
  },
  //修改昵称
  changeName() {
    wx.navigateTo({
      url: '../changeName/changeName?nickname=' + this.data.user.wx_nickname,
    })
  },
  
})
