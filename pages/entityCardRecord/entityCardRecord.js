// pages/entityCardRecord/entityCardRecord.js
var app = getApp();
let PAGE = 1;
let PAGESIZE = 10;
Page({

  /*** 页面的初始数据****/
  data: {
    list: [],//获取文章列表
    empty: '',//空数据
    hasMore: '',//是否还有更多
    loading: '',//加载中
    is_waiting:true,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    PAGE = 1;
    this.getList();
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    var that = this;
    if (!that.data.hasMore) {
      return;
    }
    PAGE++;
    that.getList();
  },
  //获取列表
  getList() {
    var that = this;
    that.setData({
      loading: true
    })
    app.ajax({
      url: 'User/WaterCard/getEntityCardRecord',
      method: "POST",
      data: {
        page: PAGE,
        row: PAGESIZE,
      },
      success: function (res) {
        let list = that.data.list;
        let oldList = that.data.list;
        let hasMore = true;
        let empty = false;
        if (res.data.code == 1000) {
          if (PAGE == 1) {
            list = res.data.data.data;
          } else {
            list = [...oldList, ...res.data.data.data];
          }
          if (list.length < res.data.data.page.total_number) {
            hasMore = false;
          }
          empty = false;
        } else {
          if (PAGE == 1) {
            list = [];
            empty = true;
          }
          hasMore = false;
        }
        that.setData({
          loading: false,
          hasMore: hasMore,
          empty: empty,
          list: list,
          is_waiting:false
        })

      }
    })
  },

})