var app = getApp();
// pages/orderDetail/orderDetail.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    data:[],
    is_waiting:false,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
      this.orderDet(options.id);
  },



  //获取订单详情
  orderDet(id){
      var that = this;
      app.ajax({
        url:"User/UserCenter/orderDetails",
        data:{
          oid:id
        },
        success:function(res){
          if(res.data.code==1000){
            var getData = res.data.data
            getData.rel_money = getData.rel_money.substr(0, getData.rel_money.length - 1);  
            getData.money = getData.money.substr(0, getData.money.length - 1); 
            getData.refund_money = getData.refund_money.substr(0, getData.refund_money.length - 1); 
            that.setData({
              data: getData,
              is_waiting:true
            })
          }else{
            app.showModal('订单信息错误!');
            setTimeout(()=>{
              that.setData({
                is_waiting: true
              })
            },2000)
          }
        }
      })
  }
})