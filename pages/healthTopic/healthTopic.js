// pages/healthTopic/healthTopic.js
var app = getApp();
let PAGE = 1;
let PAGESIZE = 10;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    zan: false, //点赞数量
    list: [], //文章列表
    empty: '', //空数据
    hasMore: '', //是否还有更多
    loading: '', //加载中
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    PAGE = 1;
    this.getNewList();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {},

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {
    var that = this;
    if(!that.data.hasMore){
      return;
    }
    PAGE++;
    that.getNewList();
  },


  // 打开新闻
  opennews: function(e) {
    wx.navigateTo({
      url: '../contentdetail/contentdetail?contentid=' + e.currentTarget.dataset.contentid,
    })
  },

  // 点赞
  dianzan: function(e) {
    var that = this;
    var zanid = e.currentTarget.dataset.zanid;
    var ups = e.currentTarget.dataset.ups;
    var indexs = e.currentTarget.dataset.indexs;
    if (ups == 1) {
      var newup = 0;
    }
    if (ups == 0) {
      var newup = 1;
    }
    that.clickzan(zanid, newup, indexs); //点赞.......
  },
  //点赞....
  clickzan: function(zanid, newup, indexs) {
    var that = this;
    var newheng = that.data.list;
    newheng[indexs].up = newup;
    app.ajax({
      url: 'User/Article/articleThumbs',
      method: "POST",
      data: {
        id: zanid
      },
      success: function(res) {
        if (res.data.code == 1000) {
          that.setData({
            list: newheng
          })
        } else {
          app.showToast("网络异常请稍后重试", "none", 2000, function() {});
        }
      }
    })
  },
  //获取新闻列表
  getNewList: function() {
    var that = this;
    that.setData({
      loading: true
    })
    app.ajax({
      url: 'User/Article/articleList',
      method: "POST",
      data: {
        page: PAGE,
        row: PAGESIZE,
        code:app.globalData.article_column_code.JK
      },
      success: function(res) {
        let list = that.data.list;
        let oldList = that.data.list;
        let hasMore = true;
        let empty = false;
        if (res.data.code == 1000) {
          if (PAGE == 1) {
            list = res.data.data;
          } else {
            list = [...oldList, ...res.data.data];
          }
          if (res.data.data.length < PAGESIZE) {
            hasMore = false;
          }
        } else {
          if (PAGE == 1) {
            list = [];
            empty = true;
          }
          hasMore = false;
        }
        that.setData({
          loading: false,
          hasMore: hasMore,
          empty: empty,
          list: list
        })
      }
    })
  }
})