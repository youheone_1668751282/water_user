// pages/integralRule/integralRule.js
var WxParse = require('../../wxParse/wxParse.js');
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    integralRuleMsg: [],
    type:0,//1签到说明 0还是普通内容
    integral_conf:{},
    task:0,//是否是任务 
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // options.type==1 签到说明
    if (options.type==1){
      wx.setNavigationBarTitle({
        title: '签到规则说明',
      })
      this.setData({ type: options.type})
      this.getsign();
    }else{
      this.integralRulefun(options.task);
      options.task==1?wx.setNavigationBarTitle({
        title: '任务规则说明',
      }):'';
    }
   
  },
  //获取签到配置
  getsign: function () {
    var that = this;
    app.ajax({
      url: 'User/Sign/integral_conf',
      data: {},
      method: 'POST',
      success: function (res) {
        if (res.data.code == 1000) {
          that.setData({
            integral_conf: res.data.data
          })
        }
      }
    })
  },

  //我知道了
  know:function(){
    wx.navigateBack({
      delta: 1
    })
  },
  //获取积分规则
  integralRulefun(RW=0){
    var that_a = this;
    app.ajax({
      url: 'User/Article/articleDetails',
      method: "POST",
      data: {
        code: RW == 1 ? app.globalData.article_column_code.RW:app.globalData.article_column_code.JF
      },
      success: function (res) {
        if (res.data.code == 1000) {
          WxParse.wxParse('desc', 'html', res.data.data.content, that_a, 5);
          that_a.setData({ task: RW})
        } else {
          app.showToast(res.data.message);
        }

      }
    })
  },
  
})