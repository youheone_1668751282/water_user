const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    card_name: "",
    card_img: "",
    current: 0, //选择的金额索引
    moneyList: [],
    innerBoxHeight: 0, //面值金额view的高度
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    const scene = decodeURIComponent(options.scene);
    if (scene) {
      let arr = scene.split(",");
      console.log(arr, "scene");
      this.share_user = arr[0]; //分享用户的id
      this.id = arr[1]; //充值卡id
      this.share_type = arr[2]; //合伙人分享进入为1，销售分享进入为2
    }
    this.loadFont();
  },

  onShow: function () {
    this.getCardInfo();
  },

  loadFont() {
    wx.loadFontFace({
      global: true,
      family: 'YouSheBiaoTiHei',
      source: `url(${app.globalData._url}font/YouSheBiaoTiHei.ttf)`,
      success: res => {
        console.log(res);
      },
      fail: e => {
        console.log(e)
      }
    })
  },

  //获取充值卡信息
  getCardInfo() {
    app.ajax({
      url: "User/Recharge/getCard",
      data: {
        id: this.id,
        share_user: this.share_user,
        share_type: this.share_type
      },
      success: res => {
        if (res.data.code == 1000) {
          const info = res.data.data;
          let card_img = info.card_img;
          let card_name = info.card_name;
          let packages = info.package;
          let moneyList = [];
          for (let x in packages) {
            moneyList.push({
              package_id: x,
              ...packages[x]
            });
          }
          let innerBoxHeight = 220 * Math.ceil(moneyList.length / 3) + 340;
          this.setData({
            card_img,
            card_name,
            moneyList,
            innerBoxHeight
          })
        } else {
          app.showToast(res.data.message);
        }
      }
    })
  },

  //=跳转规则
  navRule() {
    wx.navigateTo({
      url: '../rule/rule',
    })
  },

  //立即支付
  payRightNow() {
    if (this.repeat === 0) {
      app.showToast("正在处理中...");
      return;
    }
    this.repeat = 0;
    const money = this.data.moneyList[this.data.current];
    app.ajax({
      url: "User/Recharge/addRechargeOrder",
      data: {
        id: this.id,
        package_id: money.package_id,
        card_money: money.card_money,
        form_id: 1,
        share_user: this.share_user,
        share_type: this.share_type,
        pay_type: 1,
      },
      success: res => {
        this.repeat = 1;
        if (res.data.code == 1000) {
          console.log(res.data.data);
          const {
            timeStamp,
            signType,
            package: packages,
            nonceStr,
            paySign
          } = res.data.data;
          wx.requestPayment({
            timeStamp,
            nonceStr,
            package: packages,
            signType,
            paySign,
            success(res) {
              app.showToast("支付成功", "success");
              setTimeout(() => {
                wx.navigateTo({
                  url: '../buy-record/buy-record',
                })
              }, 1500)
            },
            fail(res) {
              app.showToast("支付失败");
            }
          })
        } else {
          app.showToast(res.data.message);
        }
      },
      error: e => {
        this.repeat = 1;
      }
    })
  },


  //选择金额
  chooseMoney(e) {
    const {
      index: current
    } = e.currentTarget.dataset;
    this.setData({
      current
    })
  },
})