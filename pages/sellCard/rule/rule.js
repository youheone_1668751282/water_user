const app = getApp();
const  WxParse = require('../../../wxParse/wxParse.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    article: "",
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    app.ajax({
      url: "Common/Common/getShareRule",
      data: {
        code: "ENI65AE1"
      },
      success: res => {
        if (res.data.code == 1000) {
          this.setData({
            article: res.data.data[0]
          })
          WxParse.wxParse('desc', 'html', res.data.data[0].content, this, 5);
        } else {
          app.showToast(res.data.message);
        }
      }
    })
  },

})