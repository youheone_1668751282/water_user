const app = getApp();
Page({

    /**
     * 页面的初始数据
     */
    data: {
        info: "",
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function(options) {
        this.id = options.id;
        this.loadFont();
    },

    onShow() {
        this.getCardInfo();
    },

    getCardInfo(id) {
        app.ajax({
            url: "Common/Common/getRechargeCard",
            data: {
                id: this.id
            },
            success: res => {
                if (res.data.code == 1000) {
                    const info = res.data.data;
                    info.headimg = info.headimg || "../../../images/logoNew.png";
                    info.wx_nickname = info.wx_nickname || `用户${info.user_id}`;
                    info.money = parseFloat(info.money) + parseFloat(info.give_money);
                    this.setData({
                        info
                    })
                } else {
                    app.showToast(res.data.message);
                }
            }
        })
    },

    loadFont() {
        wx.loadFontFace({
            global: true,
            family: 'YouSheBiaoTiHei',
            source: `url(${app.globalData._url}font/YouSheBiaoTiHei.ttf)`,
            success: res => {
                console.log(res);
            },
            fail: e => {
                console.log(e)
            }
        })
    },

    //立即领取
    receiveRightNow() {
        if (this.forbid === 1) {
            return false;
        }
        this.forbid = 1;
        app.ajax({
            url: "User/Recharge/receiveRechargeCard",
            data: {
                order_id: this.id,
                form_id: 1,
            },
            success: res => {
                if (res.data.code == 1000) {
                    this.forbid = 0;
                    app.showToast("领取成功", "success", 2000);
                    setTimeout(() => {
                        this.backHome();
                    }, 2000)
                } else {
                    app.showToast(res.data.message);
                }
            },
            error: e => {
                this.forbid = 0;
            }
        })
    },

    backHome() {
        wx.reLaunch({
            url: '../../home/home',
        })
    }


})