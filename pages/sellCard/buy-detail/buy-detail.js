const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    detail: ""
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    const {
      id
    } = options;
    this.getDetail(id);
  },


  navHome() {
    wx.reLaunch({
      url: '../../home/home',
    })
  },


  getDetail(id) {
    app.ajax({
      url: "User/Recharge/getOrderDetail",
      data: {
        id
      },
      success: res => {
        console.log(res);
        if (res.data.code == 1000) {
          this.setData({
            detail: res.data.data
          })
        } else {
          app.showToast(res.data.message);
        }
      }
    })
  },

  copy() {
    wx.setClipboardData({
      data: this.data.detail.order_number,
      success(res) {
        wx.getClipboardData({
          success(res) {
            console.log(res.data) // data
          }
        })
      }
    })
  },
})