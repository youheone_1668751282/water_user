// pages/cardManage/cardManage.js
var app = getApp();
var startX = '';//开始X坐标
var startY = '';//开始Y坐标
Page({

  /**
   * 页面的初始数据
   */
  data: {
    arrestlist: [],//获取用户卡列表
    electronList:[],//电子卡列表
    electronIndex:0,//当前选中的电子卡
    electronNum:'',//当前选中的电子卡卡号
    is_load:false,
    popshow:false,//电子卡弹窗
    modes:1,//实体卡1绑定 2新增
    is_waiting: true,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },
    /**
 * 计算滑动角度
 * @param {Object} start 起点坐标
 * @param {Object} end 终点坐标
 */
  angle: function (start, end) {
    var _X = end.X - start.X,
      _Y = end.Y - start.Y
    //返回角度 /Math.atan()返回数字的反正切值
    return 360 * Math.atan(_Y / _X) / (2 * Math.PI);
  },
    //手指触摸动作开始 记录起点X坐标
  touchstart: function (e) {
    return false;
    //开始触摸时 重置所有删除
    this.data.arrestlist.forEach(function (v, i) {
      if (v.isTouchMove)//只操作为true的
        v.isTouchMove = false;
    })
    startX = e.changedTouches[0].clientX;
    startY = e.changedTouches[0].clientY;
    this.setData({
      // startX: e.changedTouches[0].clientX,
      // startY: e.changedTouches[0].clientY,
      arrestlist: this.data.arrestlist
    })

  },
  //滑动事件处理
  touchmove: function (e) {
    return false;
    var that = this,
      index = e.currentTarget.dataset.index,//当前索引
      // startX = that.data.startX,//开始X坐标
      // startY = that.data.startY,//开始Y坐标
      touchMoveX = e.changedTouches[0].clientX,//滑动变化坐标
      touchMoveY = e.changedTouches[0].clientY,//滑动变化坐标
      //获取滑动角度
      angle = that.angle({ X: startX, Y: startY }, { X: touchMoveX, Y: touchMoveY });
    that.data.arrestlist.forEach(function (v, i) {
      v.isTouchMove = false
      //滑动超过30度角 return
      if (Math.abs(angle) > 30) return;
      if (i == index) {
        if (touchMoveX > startX) //右滑
          v.isTouchMove = false
        else //左滑
          v.isTouchMove = true
      }
    })
    //更新数据
    that.setData({
      arrestlist: that.data.arrestlist
    })
  },
  //删除水卡
  delfun: function(e){
    var that = this, inde_x = e.currentTarget.dataset.index, water_id = e.currentTarget.dataset.water_id;
    wx.showModal({
      title: '提示',
      content: '是否删除这张水卡?',
      confirmColor: '#4EB7FF',
      cancelColor: '#C4C4C4',
      success: function (res) {
        if (res.confirm) {
          app.ajax({
            url: 'User/WaterCard/deleteCard',
            method: 'POST',
            data: { id: water_id, },
            success: function (res) {
              if (res.data.code == 1000) {
                that.getWaterCard();//重新调取接口
                app.showToast('删除成功');
              } else {
                app.showToast(res.data.message);
              }
              
            }
          })
        } else {
          console.log('用户点击取消')
        }
      }
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    startX = '';//开始X坐标
    startY = '';//开始Y坐标
    this.getWaterCard();
    this.setData({ is_load: false, is_waiting:true })
  },
  //新增实体开卡记录
  entityCardRecord(){
      var that=this;
    wx.navigateTo({
      url: '../entityCardRecord/entityCardRecord'
    })
  },
  //跳转卡页面详情 cardid
  jumpDetail: function (e) {
    // return false;
    //console.log(e.currentTarget.dataset.cardid);
    wx.navigateTo({
      url: '../cardDetails/cardDetails?cardid='+ e.currentTarget.dataset.cardid
    })
  },
  //跳转消费记录
  jumpConsume: function (e) {
    //console.log(e.currentTarget.dataset.cardid);
    wx.navigateTo({
      url: '../expenseCalendar/expenseCalendar?card=' + e.currentTarget.dataset.cardid
    })
  },
  //去申请新卡 (电子卡)
  applynew:function(e){
      wx.navigateTo({
        url: '../applyElectronicCard/applyElectronicCard',
      })
  },
  //去充值
  toTopup: function (e) {
    wx.navigateTo({
      url: '../topUp/topUp?card=' + e.currentTarget.dataset.cardid,
    })
  },
  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.getWaterCard();
    this.setData({ is_load: false })
  },

  // 获取我的水卡
  getWaterCard: function () {
    // wx.showLoading()
    var that = this;
    app.ajax({
      url: 'User/WaterCard/waterCardList',
      method: "POST",
      data: {},
      success: function (res) {
        // wx.hideLoading()
        //status 1:正常 -1:注销 2:挂失
        if(res.data.code==1000){
          var getdata=res.data.data;
          var arrestlist=[];
          var electronList=[];
          getdata.forEach((item,index)=>{
            item.cards = item.card.replace(/\s/g, '').replace(/\D/g, '').replace(/(\d{4})(?=\d)/g, "$1 ");
            arrestlist.push(item);
            //is_real_card==2实体卡 其他都是电子卡
            if ((item.is_real_card == 1 && item.audit_status == 0) || (item.is_real_card == 1 && item.audit_status == 3)){
              electronList.push(item)
            }
          })
        that.setData({
          arrestlist, 
          electronList,
          electronIndex:0,
          electronNum: electronList.length > 0?electronList[0].card:'',
          electronId: electronList.length > 0?electronList[0].water_id:'',
        })
        }else{
          app.showToast(res.data.message, "none", 2000, function () { });
        }
        setTimeout(()=>{
          that.setData({ is_load: true, is_waiting: false });
        },300);
        wx.stopPullDownRefresh();
      }
    })
  },
  //打开绑定实体卡
  openMyCard(e){
    //modes==1 1绑定 2新开  data-modes="1";
    var { modes } = e.currentTarget.dataset;
    this.setData({ modes});//用来控制参数
      if (this.data.electronList.length <= 0) {
        modes == 1 ? wx.navigateTo({ url: '../binding/binding' }) : wx.navigateTo({url: '../applyCard/applyCard?card='})
      } else {
        this.setData({
          popshow: !this.data.popshow
        })
      }
  },
  none(){},//不做任何处理
  //当前选中的电子卡
  chooseElectron(e){
    var{ num,index,id} = e.currentTarget.dataset;
    this.setData({
      electronIndex: index,
      electronNum: num,
      electronId:id
    })
  },
  //去申请实体卡  1 新增 2合并
  applyTrue(e){
    var { type } = e.currentTarget.dataset;
    var electronNum = type == 2?this.data.electronNum:'';
    var id = type==2 &&this.data.electronId ? this.data.electronId:'';
    //type 1 新增 2合并
    wx.navigateTo({
      url: '../applyCard/applyCard?card=' + electronNum+'&id='+id,
    })
    this.setData({
      popshow: !this.data.popshow
    })
  },
  // 去绑定卡  1 新增 2合并
  binding: function (e) {
    var { type } = e.currentTarget.dataset;
    //type 1 新增 2合并
    var electronNum = type == 2 ? this.data.electronNum : '';
    var id = type==2&&this.data.electronId ? this.data.electronId : '';
    wx.navigateTo({
      url: '../binding/binding?card=' + electronNum + '&id=' + id,
    })
    this.setData({
      popshow: !this.data.popshow
    })
  },
  // 挂失水卡
  reportLoss: function (e) {
    var that = this;
    var { num,index } = e.currentTarget.dataset;
    wx.showModal({
      title: '是否确认挂失当前水卡?',
      content: '水卡挂失后将无法使用',
      confirmColor: '#4EB7FF',
      cancelColor: '#C4C4C4',
      success: function (res) {
        if (res.confirm) {
          that.reportsLossFun(num, index);
        } else if (res.cancel) {

        }
      }
    })
  },
  //ajax请求    挂失
  reportsLossFun: function (num, index) {
    var that = this;
    app.ajax({
      url: 'User/WaterCard/reportLoss',
      method: "POST",
      data: { card: num },
      success: function (res) {
        if (res.data.code == 1000) {
          var arrestlist= that.data.arrestlist;
          arrestlist[index].status=2;
          that.setData({ arrestlist})
        } else {
          app.showToast(res.data.message, "none", 2000, function () { });
        }
      }
    })
  },
  // 解除挂失水卡状态
  relieve: function (e) {
    var that = this;
    var { num, index } = e.currentTarget.dataset;
    wx.showModal({
      title: '是否确认解除水卡挂失?',
      content: '解除挂失后可立即使用',
      confirmColor: '#4EB7FF',
      cancelColor: '#C4C4C4',
      success: function (res) {
        if (res.confirm) {
          that.rescissionLoss(num,index);
        } else if (res.cancel) {

        }
      }
    })
  },
  //ajax 解除挂
  rescissionLoss: function (num,index) {
    var that = this;
    app.ajax({
      url: 'User/WaterCard/rescissionLoss',
      method: "POST",
      data: { card: num },
      success: function (res) {
        if (res.data.code == 1000) {
          var arrestlist = that.data.arrestlist;
          arrestlist[index].status = 1;
          that.setData({ arrestlist })
        } else {
          app.showToast(res.data.message, "none", 2000, function () { });
        }
      }
    })
  },
})