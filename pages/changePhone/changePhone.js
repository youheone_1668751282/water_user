// pages/changePhone/changePhone.js
var app = getApp();
var open=true
Page({
  /**
   * 页面的初始数据
   */
  data: {
    time: '获取验证码',
    disabled: false,
    currentTime: 60,
    phone: '',   // 新手机号码
    code: '',  // 验证码
    oldPhs:'',//原来手机号码
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.setData({
      oldPhs: options.phone ? options.phone : ''
    })
  },  

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  // 提交修改
  editeNme: function () {
    var that = this;
    var phone = that.data.phone;
    var code = that.data.code;
    // ajax请求
    app.ajax({
      url: 'User/User/editUserInfo',
      data: { phone: phone, code: code },
      success: function (res) {
        if (res.data.code == 1000) {
          app.showToast(res.data.message, "none", 2000, function () { });
          setTimeout(() => {
            wx.navigateBack({
              delta: 1
            })
          }, 1000);
        } else {
          app.showToast(res.data.message, "none", 2000, function () { });
        }
      }
    })
  },

  // 手机号码输入
  phoneinput(e) {
    var that = this;
    that.setData({
      phone: e.detail.value
    })
  },

  // 验证码输入
  codeinput(e) {
    var that = this;
    that.setData({
      code: e.detail.value
    })
  },

  // 获取验证码
  getCode: function(options) {
    var that = this;
    var currentTime = that.data.currentTime;
    var phone_num = that.data.phone;
    var numreg = /^[1][0-9][\d]{9}$/;

    if (phone_num == '') {
      app.showToast("手机号码不能为空", "none", 2000, function() {});
      return false;
    }
    if (phone_num == that.data.oldPhs){
      app.showToast("与原来手机号码一致", "none", 2000, function () { });
      return false;
    }
    if (numreg.test(phone_num) == false) {
      app.showToast("请输入正确手机的号码", "none", 2000, function() {});
      return false;
    }
    if(!open){
      return false
    }
    open = false
    // ajax请求
    app.ajax({
      url: 'User/Login/send_sms',
      data: { phone: phone_num },
      success: function (res) {
        app.showToast(res.data.message, "none", 2000, function () { });
        if (res.data.code == 1000) {
          that.setData({
            time: currentTime + '秒',
            disabled: true
          })
          var interval = setInterval(function () {
            that.setData({
              time: (currentTime - 1) + '秒',
              disabled:true
            })
            currentTime--;
            if (currentTime <= 0) {
              clearInterval(interval)
              that.setData({
                time: '重新获取',
                currentTime: 60,
                disabled: false
              })
            }
          }, 1000)
        } else {
          app.showToast("发送失败，请稍后再试", "none", 2000, function () { });
        }
        setTimeout(()=>{
          open = true
        },2000)
        console.log('发送短信结果',res)
      }
    })
  }

})