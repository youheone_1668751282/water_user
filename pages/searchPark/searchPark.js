// pages/searchPark/searchPark.js
var app = getApp();
var areaIdInfo = [];//选中的位置信息
let PAGE = 1;
let PAGESIZE = 20;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    areaName:'',
    village_name:'',
    showpop:false,
    pupshow:false,
    villagelist:[],
    village_id:'',

    empty: '', //空数据
    hasMore: '', //是否还有更多
    loading: '', //加载中
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    PAGE = 1;
    console.log(options.area0, options.area1, options.area2)
    if (options.area0){
      areaIdInfo = [options.area0, options.area1, options.area2];
      }
    var pages = getCurrentPages();
    var currPage = pages[pages.length - 1]; //当前页面
    var prevPage = pages[pages.length - 2];//上一个页面 
    if (prevPage.data.village_name || wx.getStorageSync('lat') && wx.getStorageSync('lng')){
        this.setData({
          village_name: prevPage.data.village_name,
          village_id: prevPage.data.village_id
        })
      this.getVillageMsg();
    }
    // if (options.village_name){
    //     this.setData({
    //       village_name: options.village_name
    //     })
    //   this.getVillageMsg();
    // }
  },
  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {
    var that = this;
    if (!that.data.hasMore) {
      return;
    }
    PAGE++;
    this.getVillageMsg();
  },
  //输入园区名称监听
  villageName: function (event) {
    var that = this;
    if (event.detail.value == '') {
      that.setData({
        showpop: false,
        pupshow: false,
      })
    }
    that.setData({
      village_name: event.detail.value
    })
    that.getVillageMsg();
  },
  //点击获取园区信息
  getMsgv() {
    var that = this;
    that.setData({
      showpop: false,
    })
    if (areaIdInfo.length == 0) {
      app.showToast("请返回,先确定位置信息", "none", 2000, function () { });
    } else {
      that.searchVillage();//设备搜索
    }
  },
  //获取园区名称列表
  getVillageMsg: function () {
    var that = this;
    if (areaIdInfo.length == 0) {
      //app.showModal('请先确定位置信息', '', function () { });
      app.showToast("请先确定位置信息", "none", 2000, function () { });
    }  else {
      //发起ajax请求
      that.searchVillage();
    }

  },
  //搜索园区列表ajax请求  
  searchVillage: function () {
    var that = this;
    var v_name = that.data.village_name;
    var p_id = areaIdInfo[0];
    var c_id = areaIdInfo[1];
    var a_id = areaIdInfo[2];
    var lat = wx.getStorageSync('lat');
    var lng = wx.getStorageSync('lng');
    app.ajax({
      url: 'Common/Common/searchVillage',
      method: "POST",
      data: {
        province: p_id, city: c_id, area: a_id, village_name: v_name, latitude: lat, longitude: lng, page: PAGE, row: PAGESIZE},
      success: function (res) {
        let list = that.data.villagelist;
        let oldList = that.data.villagelist;
        let hasMore = true;
        let empty = false;
        if (res.data.code == 1000) {
          if (PAGE == 1) {
            if (res.data.data != '') {
              list = res.data.data;
              empty = false;
            } else {
              list = [];
              empty = true;
              // app.showToast("未查询到园区,请重试", "none", 2000, function () { });
            }
          } else {
            empty = false;
            list = [...oldList, ...res.data.data];
          }
          // 是否加载更多
          if (res.data.data.length < PAGESIZE) {
            hasMore = false;
          }
          

          that.setData({
            villagelist: list,//获取园区信息列表
            // showpop: true//展示下拉选项
            pupshow: true,
            loading: false,
            hasMore: hasMore,
            empty: empty,
          })
          
        } else {
          if (PAGE == 1) {
            list = [];
            empty = true;
          }
          hasMore = false;
          that.setData({
            loading: false,
            hasMore: hasMore,
            empty: empty,
            villagelist: list
          })
          app.showToast(res.data.message, "none", 2000, function () { });
        }
      }
    })
  },
  //选择那一台设备
  chooseOnit(e) {
    var that = this;
    that.setData({
      pupshow: false
    });
    var village_name = e.currentTarget.dataset.village_name;
    var village_id =e.currentTarget.dataset.id;
    var pages = getCurrentPages();
    var currPage = pages[pages.length - 1]; //当前页面
    var prevPage = pages[pages.length - 2];//上一个页面 
    prevPage.setData({//本页面数据赋值到上一个页面
      village_name, showpop: false,
      village_id
    })
    wx.navigateBack({
      delta:1
    })
  }
})