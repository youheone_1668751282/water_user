// pages/user/changeName/changeName.js
var app=getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    name:''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    if (options.nickname) {
      that.setData({
        name: options.nickname||'',
      })
    } else {
      that.setData({
        name: '',
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },
  // 输入昵称
  inputNameFun: function (e) {
    var name = e.detail.value;
    this.setData({
      name: name
    });
  },
  //修改名称
  editeNme(){
    var that=this;
    var name=that.data.name;
    if (name.length<2){
      app.showToast('昵称不能少于两位', "none", 2000, function () { });
      return false;
    }
    app.ajax({
      url: "User/User/editUserInfo",
      data: { username: name},
      success: function (res) {
        console.log('获取用户信息', res)
        if (res.data.code == 1000) {
          app.showToast(res.data.message, "none", 2000, function () { });
          setTimeout(() => {
            wx.navigateBack({
              delta: 1
            })
          }, 1000);
        } else {
          app.showToast(res.data.message, "none", 2000, function () { });
        }
      }
    })
    
  }
})