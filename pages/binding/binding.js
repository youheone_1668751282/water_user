// pages/binding/binding.js
var  app=getApp();
var cando=true;
var countdowns = 120;
var regetc = '获取验证码';
var settime = function (that) {
  if (countdowns == 0) {
    that.setData({
      is_show: true
    })
    countdowns = 120;
    regetc = '重新获取';
    return;
  } else {
    that.setData({
      is_show: false,
      last_time: countdowns
    })
    countdowns--;
    regetc = '重新获取';
  }
  setTimeout(function () {
    settime(that)
  }
    , 1000)
}
Page({

  /**
   * 页面的初始数据
   */
  data: {
    card_num:'',//实体卡卡号
    card_show:'',//展示的卡号
    phoneCode:'',//验证码
    showcode: regetc,//内容
    last_time: '',//剩余时间
    is_show: true,//是否显示倒计时
    cardList:[],//获取我的未绑定卡列表
    chooseSardid:'',//选择的卡ID
    waterCardUserPhone: '',//水卡用户手机号
    txt:'请先输入卡号或扫描二维码',
    electronCard:'',//电子卡卡号
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var electronCard = options.card ? options.card:'';
    var chooseSardid = options.id ? options.id:'';
    this.setData({
      electronCard,
      chooseSardid
    })
  },
  //输入绑定值
  inputcardnum:function(e){
    e.detail.value.length <= 0 ? this.setData({ txt:'请先输入卡号或扫描二维码'}):'';
    this.setData({
      card_num: e.detail.value.replace(/\s*/g, ""),
      card_show: e.detail.value.replace(/\s/g, '').replace(/\D/g, '').replace(/(\d{4})(?=\d)/g, "$1 ")
    })
  },

  //选择扫码
  scanBtn: function () {
    var that=this;
    // 只允许从相机扫码 result(所扫码的内容) scanType(所扫码的类型) charSet(所扫码的字符集) path(当所扫的码为当前小程序的合法二维码时，会返回此字段，内容为二维码携带的 path)
    wx.scanCode({
      onlyFromCamera: true,
      success: (res) => {
        //console.log(res)
        that.setData({
          card_num: res.result,
          card_show: res.result.replace(/\s/g, '').replace(/\D/g, '').replace(/(\d{4})(?=\d)/g, "$1 ")
        })
        that.getWaterCardInfo();
      },
      fail:function(e){
        //console.log(e)
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.fictitiousCard();
     cando = true;
  },

  //获取用户电子水卡信息 
  fictitiousCard:function(){
    var that = this;
    app.ajax({
      url: "User/WaterCard/fictitiousCard",
      data: {},
      success: function (res) {
        if (res.data.code == 1000) {
          // that.setData({
          //   cardList: res.data.data
          // });
        } else {
          app.showModal(res.data.message);
        }
      }
    })
  },
  //获取水卡的用户信息
  getWaterCardInfo(){
    var that = this;
    if (that.data.card_num.length<10){
      app.showToast('卡号位数不能小于10位!')
      return
    }
    app.ajax({
      url: 'User/WaterCard/getCardPhone',
      data: {
        card: that.data.card_num
      },
      success: function (res) {
        if (res.data.code == 1000) {
          var tel = res.data.data.tel;
          var reg = /^(\d{3})\d{4}(\d{4})$/;  
          tel = tel.replace(reg, "$1****$2");
          that.setData({
            waterCardUserPhone: res.data.data.tel,
            txt: '验证码将发送到手机号： ' + ' '+ tel +' '+" (如手机号码不正确请联系客服)"
          });
        } else {
          that.setData({
            waterCardUserPhone: '',
            txt:res.data.message
          });
          // app.showModal(res.data.message);
        }
      }
    })
  },
  //验证码输入
  getcode:function(e){
      this.setData({
        phoneCode: e.detail.value
      })
  },
  //获取验证码 send_sms
  getphonecode:function(){
    var that = this;
    var card_num = that.data.card_num;
    if (card_num==''){
      app.showToast("请输入卡号或者扫描卡号");
      return false
    }
    app.ajax({
      url: "User/WaterCard/send_sms",
      data: { card: card_num},
      success: function (res) {
        if (res.data.code == 1000) {
          that.setData({
            is_show: (!that.data.is_show),  //false
            showcode: regetc,
            txt: res.data.message
          })
          settime(that); //刷新倒计时 
          app.showToast(res.data.message, "none", 2500, function () { });
        } else {
          app.showToast(res.data.message);
        }
      }
    })
  },
  //绑定水卡  bindWaterCard
  bindWaterCard:function(){
    var that = this;
    var card_num=that.data.card_num;
    var chooseSardid=that.data.chooseSardid;
    var phoneCode = that.data.phoneCode;
    if (card_num==''){
      app.showToast('请输入或扫描实体卡卡号');
      return false;
    }
    if (phoneCode==''){
      app.showToast('请先获取验证码');
      return false;
    }
    if(!cando){
      return false;
    }
    cando=false;
    app.ajax({
      url: "User/WaterCard/bindWaterCard",
      data: { card: card_num, id: chooseSardid, code: phoneCode },
      success: function (res) {
        console.log(res)
        if (res.data.code == 1000) {
          // that.setData({
          //   cardList: res.data.data
          // });
          app.showToast(res.data.message);
          setTimeout(function () {
            cando = true;
            wx.navigateBack({
              delta: 1
            })
          },2000);
        } else {
          app.showToast(res.data.message);
        }
        setTimeout(function () {
          cando=true;
        }, 2000);

        
      }
    })
  },
  //判断是否获取验证码
  codeFocus(){
    if (!this.data.card_num){
      app.showToast('请先获取卡号');
    }
  }
})