// pages/changeRecord/changeRecord.js
var app = getApp();
let PAGE = 1;
let PAGESIZE = 20;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    eqnumber:'',//设备编号
    operationlist:[],//获取列表
    empty: '',//空数据
    hasMore: '',//是否还有更多
    loading: '',//加载中
    is_load:false
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options);
    var eqnumber = options.eqnumber;
    var id = options.id;
    this.setData({
      eqnumber: eqnumber,
      id:id
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var that = this;
    that.setData({
      operationlist: []
    })
     PAGE = 1;
    that.getchangeRecord();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },
  //获取换芯记录
  getchangeRecord: function (){
  var that=this;
  var eqnumber = that.data.eqnumber
  var id = that.data.id;
  app.ajax({
    url: 'User/WaterStation/operationlist',
    data: { id: id, equipment_no: eqnumber, page: PAGE, row: PAGESIZE },
    method: 'POST',
    success: function (res) {
      // console.log(res.data.data);
      let list = that.data.operationlist;
      let oldList = that.data.operationlist;
      let hasMore = true;
      let empty = false;
      if (res.data.code == 1000) {
        if (PAGE == 1) {
          list = res.data.data.operationlist;
        } else {
          list = [...oldList, ...res.data.data.operationlist];
        }
        if (res.data.data.operationlist.length < PAGESIZE) {
          hasMore = false;
        }
        empty = false;
      } else {
        if (PAGE == 1) {
          list = [];
          empty = true;
        }
        hasMore = false;
      }
      that.setData({
        loading: false,
        hasMore: hasMore,
        empty: empty,
        operationlist: list
      })
      that.setData({ is_load:true})
    }
  })
  },
  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    // var that = this;
    // page_s += 1;
    // // 当没有数据时,不再请求
    // if (that.data.is_request == 1) {
    //   app.showToast("~_~ 到底了,别扯了!", "none", 2000, function () { });
    // } else {
    //   this.getchangeRecord(page_s);
    // }
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    var that = this;
    if (!that.data.hasMore) {
      return;
    }
    PAGE++;
    that.getchangeRecord();
  } 

})