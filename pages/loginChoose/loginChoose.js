// loginChoose/loginChoose
var app = getApp();


// pages/home/home.js 
Page({
  /**
   * 页面的初始数据
   */
  data: {
    logo:''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
   
    this.getCompanyImg();
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var that = this;
    wx.setStorageSync('is_login', false);
    app.login();
    this.hidebuttonFun();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  }, 
  //现获取基础库版本
  hidebuttonFun() {
    wx.getSystemInfo({
      success: function (res) {
        var version = res.SDKVersion;
        version = version.replace(/\./g, "")
        console.log('结果', version)
        if (parseInt(version) >= 283) {
          wx.hideHomeButton();
        }
      }
    })
  },
  // 获取logo
  getCompanyImg: function () {
    var that = this;
    var company_no = app.globalData.company_no
    // ajax请求
    app.ajax({
      url: 'Api/Company/getCompayConfig',
      url_type: 1,
      data: { company_no },
      success: function (res) {
        if (res.data.code == 1) {
          that.setData({
            logo: res.data.data.img_config.image
          })
        }
      }
    })
  },
  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    console.log('页面卸载');
    wx.setStorageSync('is_first', true);
  },  

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },
  //获取用户手机号码
  getPhoneNumber: function (e) {
    console.log(123,e)
    var iv = e.detail.iv;
    var encryptedData = e.detail.encryptedData;
    var that = this;
    if (e.detail.errMsg == 'getPhoneNumber:ok'){
      that.getAjaxPhone(iv, encryptedData);//后台解析手机信息
    } else{
      app.showToast('拒绝授权,将无法使用一键登录功能', "none", 2000, function () { });
    }
    // if (e.detail.errMsg == 'getPhoneNumber:fail:cancel to confirm login' || e.detail.errMsg == 'getPhoneNumber:fail user deny') {
    //   app.showToast('拒绝授权,将无法使用一键登录功能', "none", 2000, function () { });
    // } else {
    //   that.getAjaxPhone(iv, encryptedData);//后台解析手机信息
    // }
  },
  //后台解析手机号码
  getAjaxPhone(iv, encryptedData) {
    var that = this;
    var session_key = wx.getStorageSync('session_key');
    app.ajax({
      url: "User/Wx/aesPhoneDecrypt",
      data: { key: session_key, iv: iv, data: encryptedData },
      success: function (res) {
        if (res.data.code == 1000) {
          wx.setStorageSync('phone', res.data.data.phoneNumber);
          wx.setStorageSync('is_way',1);
          console.log('手机号码', res.data.data.phoneNumber)
          wx.setStorageSync('token', '');//清空本地token
          app.getToken();//重新获取token 保存手机号码重新登录
        } else {
          app.showToast('用户信息解析失败请重新授权', "none", 2000, function () { });
          return;
        }
      }
    })
  },
  //跳转手机验证码登陆
  jumpLogin(){
    wx.navigateTo({
      url: '../login/login',
    })
  },
  //跳转手机验证码登陆
  jumpLogin() {
    wx.navigateTo({
      url: '../login/login',
    })
  }

})