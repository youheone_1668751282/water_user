// pages/awardRecord/awardRecord.js
var app = getApp();
var PAGESIZE= 18;//页码数据
var PAGE= 1;//页码
Page({

  /**
   * 页面的初始数据
   */
  data: {
    onchoose:1,//默认是积分抽奖
    winningListMsg:[],//中奖记录
    empty: '',//空数据
    hasMore: '',//是否还有更多
    loading:'',//加载中
    is_load:false
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.setData({
      winningListMsg: [],
      is_load:false,
    })
    PAGE=1
    this.getwinningList();//获取中奖记录
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    var that = this;
    if(!that.data.hasMore){
      return;
    }
    PAGE += 1;    
    this.getwinningList();

  },
  //积分中奖还是实物中奖
  chooseit:function(e){
    PAGE=1;
    //console.log(e.target.dataset.onit)
    this.setData({
      onchoose: e.currentTarget.dataset.onit,//切换table
    })
    if (e.currentTarget.dataset.onit==1){
      this.setData({
        winningListMsg: [],   // 清空列表数据
      })
      this.getwinningList();
    }else{
      this.setData({
        winningListMsg: [],   // 清空列表数据
        hasMore: false,
        empty: true,
      })
    }
   
  },
  //去抽奖
  jumpcj:function(){
    var pages = getCurrentPages();
    var currPage = pages[pages.length - 1];   //当前页面
    var prevPage = pages[pages.length - 2];  //上一个页面
    if (prevPage.route =='pages/lottery/lottery'){
      wx.navigateBack({
        delta:1
      })
    }else{
      wx.navigateTo({
        url: '../lottery/lottery',
      })
    }
    
  },
  //获取用户抽奖记录 User/Activity/winningList
  getwinningList: function (){
    var that=this;
    that.setData({
      loading: true
    })
    app.ajax({
      url: 'User/Activity/winningList',
      method: "POST",
      data: { row: PAGESIZE, page: PAGE, prize_type:0},
      success: function (res) {
        let list = that.data.winningListMsg;
        let oldList = that.data.winningListMsg;
        let hasMore = true;
        let empty = false;
        if (res.data.code==1){
          if(PAGE == 1){
            list = res.data.data;
          }else{
            list = oldList.concat(res.data.data);
          }
          if(res.data.data.length<PAGESIZE){
            hasMore = false;
          }
          empty = false;
        }else{
          if(PAGE == 1){
            list = [];
            empty = true;
          }
          hasMore = false;
        }
        that.setData({
          is_load:true,
          loading: false,
          hasMore: hasMore,
          empty: empty,
          winningListMsg: list
        })   
      }
    })  
  },
})