// pages/aboutWe/aboutWe.js
var WxParse = require('../../wxParse/wxParse.js');
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
      aboutmsg:[]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getAboutwe();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },
  //获取新闻列表
  getAboutwe: function () {
    var that_a = this;
    app.ajax({
      url: 'User/Article/articleDetails',
      method: "POST",
      data: {code:app.globalData.article_column_code.GY},
      success: function (res) {
        if (res.data.code == 1000) {
          that_a.setData({
            aboutmsg: res.data.data
          })
          WxParse.wxParse('desc', 'html', res.data.data.content, that_a, 5);
        } else {
          app.showToast("网络错误,请稍后重试");
        }

      }
    })
  }


})