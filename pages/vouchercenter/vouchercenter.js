var app = getApp();
let noMore=false;
// pages/vouchercenter/vouchercenter.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    couponList:[],
    page:1,
    pageSize:30,
    flag:true,
    type:'',//pay 从扫码打水选择领取优惠券 进入该页面
    is_load: false,//是否加载
    code:'',//兑换码
    popshow:false,
    is_waiting: true,//加载动画
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getCoupon();
    this.setData({
      type: options.type||'',
      is_waiting:true,
    })
  },
  onUnload:function(){
    noMore=false;
  },
  //获取优惠券列表
  getCoupon(){
    var that = this;    
    app.ajax({
      url: 'User/Coupon/couponList',
      data: {
        page:that.data.page,
        pageSize:that.data.pageSize
      },
      method: 'POST',
      success: function (res) {
        if (res.data.code == 1000) {
          if (res.data.data.length < that.data.pageSize){
            noMore=true;
          }
          var list = that.data.couponList;
          res.data.data.forEach(function(v,k){
            list.push(v);
          })
          console.log(list);
          that.setData({
            couponList: list
          })
        }
        setTimeout(()=>{
          that.setData({ is_load: true, is_waiting: false });
        },1000)
        
      }
    })
  },
  Receive(e){
    var that = this;
    var type = that.data.type;
    console.log(e);
    var coupon_id = e.target.dataset.id;
    if(!that.data.flag){
      return false;
    }
    that.setData({
      flag:false
    })
    app.ajax({
      url: 'User/Coupon/receiveCoupon',
      data: {
        coupon_id: coupon_id
      },
      method: 'POST',
      success: function (res) {
       
        that.setData({
          flag: true
        })
        if (res.data.code == 1000){
          if (type == 'pay') {
            wx.showModal({
              title: '已成功领取优惠券',
              content: '是否返回支付页面使用优惠券？',
              cancelText: '继续领券',
              success(res) {
                if (res.confirm) {
                  wx.setStorageSync('isBack', true);
                  wx.navigateBack({
                    delta: 1
                  })
                }
                if (res.cancel) {

                }
              }
            })
          } else {
            // that.setData({
            //   couponList: [],
            //   page: 1,
            //   is_load: false
            // });
            // noMore = true;
            // that.getCoupon();
            var couponList = that.data.couponList;
            var index = e.target.dataset.index;
            couponList[index].user_get = 1;
            that.setData({
              couponList
            })
          }
        }else{
          app.showToast(res.data.message);
        }
        
        
      }
    })
  },
  //优惠券已抢光
  full(){
      app.showToast('已抢光了~');
      return false;
  },
  //立即使用
  nowUse(){
    // wx.navigateBack({
    //   delta: 1
    // })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },


  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    if(noMore){
      return;
    }
    var that = this;
    var page = that.data.page+1;
    that.setData({
      page:page
    })
    that.getCoupon();
  },

  checkDatail() {
    wx.navigateTo({
      url: '../receivedcoupons/receivedcoupons',
    })
  },
  none() { },//不做任何处理
  //打开兑换优惠券
  openPop(){
    this.setData({
      popshow: !this.data.popshow
    })
  },
  //输入兑换
  setCode(e) {
    var code = e.detail.value;
    this.setData({
      code
    })
  },
  //兑换优惠券
  exchange() {
    var that = this;
    app.ajax({
      url: 'User/Coupon/exchangeCoupon',
      data: {
        code: that.data.code
      },
      method: 'POST',
      success: function (res) {
        if (res.data.code == 1000) {
          that.setData({
            popshow:false,
            page: 1,
            couponList: [],
          })
          app.showToast(res.data.message);
          that.getCoupon();
        } else {
          app.showToast(res.data.message);
        }
      }
    });
  },
})
